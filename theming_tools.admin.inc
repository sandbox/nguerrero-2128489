<?php

/**
 * The admin settings form for Theming Tools.
 */
function theming_tools_admin_settings() {
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );
  $form['settings']['theming_tools_css_browser_selector'] = array(
    '#type' => 'radios',
    '#title' => t('Use Css Browser selector'),
    '#description' => t(''),
    '#options' => array(
      0 => 'Disable',
      1 => 'JS Method',
      2 => 'PHP Method',
    ),
    '#default_value' => variable_get('theming_tools_css_browser_selector', 0),
  );
  $form['settings']['theming_tools_refresh_css'] = array(
    '#type' => 'radios',
    '#title' => t('Use Refresh CSS'),
    '#description' => t(''),
    '#options' => array(
      0 => 'Disable',
      1 => 'Enable',
    ),
    '#default_value' => variable_get('theming_tools_refresh_css', 0),
  );
  $form['settings']['theming_tools_naked'] = array(
    '#type' => 'radios',
    '#title' => t('Use Naked Day'),
    '#description' => t(''),
    '#options' => array(
      0 => 'Disable',
      1 => 'Enable',
    ),
    '#default_value' => variable_get('theming_tools_naked', 0),
  );
  $form['settings']['theming_tools_ie_must_die'] = array(
    '#type' => 'radios',
    '#title' => t('Use IE MUST DIE'),
    '#description' => t(''),
    '#options' => array(
      0 => 'Disable',
      1 => 'Enable',
    ),
    '#default_value' => variable_get('theming_tools_ie_must_die', 0),
  );
  $form['settings']['theming_tools_selectivizr'] = array(
    '#type' => 'radios',
    '#title' => t('Use Selectivizr'),
    '#description' => t(''),
    '#options' => array(
      0 => 'Disable',
      1 => 'Enable',
    ),
    '#default_value' => variable_get('theming_tools_selectivizr', 0),
  );
  $form['settings']['theming_tools_html5_outline'] = array(
    '#type' => 'radios',
    '#title' => t('Use HTML5 OUTLINE'),
    '#description' => t(''),
    '#options' => array(
      0 => 'Disable',
      1 => 'Enable',
    ),
    '#default_value' => variable_get('theming_tools_html5_outline', 0),
  );
  $form['settings']['theming_tools_class_variables'] = array(
    '#type' => 'hidden',
    '#title' => t('Class variables'),
    '#default_value' => variable_get('theming_tools_class_variables', 'classes_array'),
  );

  return system_settings_form($form);
}
