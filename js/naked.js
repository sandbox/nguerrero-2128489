/*
 * jQuery.nakedDay - for the CSS Naked Day
 *
 * Licensed under the MIT license.
 */
 
Drupal.behaviors.nakedCss = {
  attach: function () {
    (function(){
      var now = new Date();
      var time = now.getTime();
      var start = new Date(now.getFullYear(), 3, 9, 0, 0, 0).getTime();
      var end = new Date(now.getFullYear(), 3, 10, 0, 0, 0).getTime();
      if (time >= start && time <= end){
        jQuery('link[rel=stylesheet], style').remove();
        jQuery(window).bind("load", function() {
          jQuery('*').each(function(e) {
            jQuery(this).removeAttr('style');
          });
        });
      }
    })();
  }
};
